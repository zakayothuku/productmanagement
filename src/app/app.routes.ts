import {RouterModule, Routes} from '@angular/router';
import {ProductsComponent} from './products/products.component';
import {ViewProductComponent} from './products/view-product/view-product.component';
import {AddProductComponent} from './products/add-product/add-product.component';
import {EditProductComponent} from './products/edit-product/edit-product.component';

const routes: Routes = [
  {path: 'products', component: ProductsComponent},
  {path: 'products/add', component: AddProductComponent},
  {path: 'products/edit/:id', component: EditProductComponent},
  {path: 'products/:id', component: ViewProductComponent},
  {path: '', redirectTo: '/products', pathMatch: 'full'}
];

export const Router = RouterModule.forRoot(routes);
