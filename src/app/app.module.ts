import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {SlimLoadingBarModule} from 'ng2-slim-loading-bar';
import {LayoutModule} from '@angular/cdk/layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {AppComponent} from './app.component';
import {Router} from './app.routes';
import {ProductsService} from './_services/products.service';
import {ProductsComponent} from './products/products.component';
import {ViewProductComponent} from './products/view-product/view-product.component';
import {EditProductComponent} from './products/edit-product/edit-product.component';
import {AddProductComponent} from './products/add-product/add-product.component';
import {
  MatButtonModule,
  MatCardModule,
  MatDialogModule,
  MatFormFieldModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSelectModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatToolbarModule
} from '@angular/material';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CategoriesService} from './_services/categories.service';
import {SidebarComponent} from './dashboard/sidebar/sidebar.component';
import {NavbarComponent} from './dashboard/navbar/navbar.component';
import {SpinnerComponent} from './_common/spinner.component';
import {CdkColumnDef} from "@angular/cdk/table";

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    ViewProductComponent,
    EditProductComponent,
    AddProductComponent,
    DashboardComponent,
    ProductsComponent,
    ViewProductComponent,
    EditProductComponent,
    AddProductComponent,
    DashboardComponent,
    NavbarComponent,
    SidebarComponent,
    SpinnerComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    Router,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    SlimLoadingBarModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatGridListModule,
    MatMenuModule,
    LayoutModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatDialogModule
  ],
  providers: [ProductsService, CategoriesService, CdkColumnDef],
  bootstrap: [AppComponent]
})
export class AppModule {
}
