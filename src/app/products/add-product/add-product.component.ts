import {Component, OnInit} from '@angular/core';
import {Location} from '@angular/common';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Category} from '../../_models/category';
import {CategoriesService} from '../../_services/categories.service';
import {ProductsService} from '../../_services/products.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  productForm: FormGroup;
  categoryControl = new FormControl(null, [Validators.required]);

  name = '';
  description = '';
  price = 0.0;

  categories: Category[];

  constructor(private router: Router,
              private location: Location,
              private categoriesService: CategoriesService,
              private productsService: ProductsService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {
    this.categoriesService.getCategories().subscribe(res => {
      console.log(res);
      this.categories = res;
    }, err => {
      console.log(err);
    });

    this.productForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required],
      'price': [null, Validators.required],
      'category_id': [null, Validators.required]
    });
  }

  onFormSubmit(form: NgForm) {
    this.productsService.addProduct(form).subscribe(res => {
      console.log(res);
      this.router.navigate(['products']);
    }, (err) => {
      console.log(err);
    });
  }

  back() {
    this.location.back();
  }

}
