import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {ProductsService} from '../_services/products.service';
import {Product} from '../_models/product';
import {Router} from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
})
export class ProductsComponent implements OnInit, AfterViewInit {

  products: Product[];

  dataSource = new MatTableDataSource();

  displayedColumns = ['id', 'name', 'description', 'price', 'actions'];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('input') input: ElementRef;

  constructor(private router: Router, private productsService: ProductsService) {
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(
      products => this.dataSource.data = products
    );
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  addProduct(): void {
    this.router.navigate(['products/add']);
  }

  editProduct(row): void {
    this.router.navigate(['products/edit/', row.id]);
  }

  deleteProduct(row): void {
    this.productsService.deleteProduct(row.id).subscribe(() => {
      console.log("Deleted!");
    }, err => {
      console.log(err);
    });
  }

  onProductClicked(row) {
    this.router.navigate(['products/', row.id]);
  }

  applyFilter(value: string) {
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;
  }
}
