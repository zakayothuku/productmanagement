import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ProductsService} from '../../_services/products.service';
import {Product} from "../../_models/product";
import {CategoriesService} from "../../_services/categories.service";
import {Category} from "../../_models/category";
import {Location} from "@angular/common";

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  product: Product;
  category: Category;

  constructor(private router: Router,
              private location: Location,
              private route: ActivatedRoute,
              private productsService: ProductsService,
              private categoriesService: CategoriesService) {
  }

  ngOnInit() {
    this.getProductDetails(this.route.snapshot.params['id']);
  }

  getProductDetails(id) {
    this.productsService.getProduct(id).subscribe(data => {
      console.log(data);
      this.product = data;

      this.categoriesService.getCategory(data.id).subscribe(res => {
        console.log(res);
        this.category = res;
      }, err => {
        console.log(err);
      });

    });
  }

  back() {
    this.location.back();
  }

  gotoProducts() {
    this.router.navigate(['products/']);
  }

}
