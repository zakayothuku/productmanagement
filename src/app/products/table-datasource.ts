import {CollectionViewer, DataSource} from '@angular/cdk/collections';
import {catchError, finalize} from 'rxjs/internal/operators';
import {of} from 'rxjs/index';
import {BehaviorSubject, Observable} from 'rxjs/Rx';
import {ProductsService} from '../_services/products.service';
import {Product} from '../_models/product';

export class TableDataSource extends DataSource<any> {

  public productsCount = 0;
  private productsBehavior = new BehaviorSubject<Product[]>([]);
  private loadingBehavior = new BehaviorSubject<boolean>(false);
  public loading$ = this.loadingBehavior.asObservable();

  constructor(private productsService: ProductsService) {
    super();
  }

  connect(collectionViewer: CollectionViewer): Observable<Product[]> {
    return this.productsBehavior.asObservable();
  }

  disconnect(collectionViewer: CollectionViewer): void {
    this.productsBehavior.complete();
    this.loadingBehavior.complete();
  }

  fetchProducts() {

    this.loadingBehavior.next(true);

    this.productsService.getProducts()
      .pipe(
        catchError(() => of([])),
        finalize(() => this.loadingBehavior.next(false))
      )
      .subscribe(products => {
        this.productsCount = products.length;
        this.productsBehavior.next(products);
      });
  }

}
