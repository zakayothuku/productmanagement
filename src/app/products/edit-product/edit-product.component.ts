import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {Category} from '../../_models/category';
import {ProductsService} from '../../_services/products.service';
import {CategoriesService} from '../../_services/categories.service';
import {ActivatedRoute, Router} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  productForm: FormGroup;
  categoryControl = new FormControl('', [Validators.required]);

  id = 0;
  name = '';
  description = '';
  price = 0;
  category_id = '';

  categories: Category[];

  constructor(private router: Router,
              private location: Location,
              private route: ActivatedRoute,
              private categoriesService: CategoriesService,
              private productsService: ProductsService,
              private formBuilder: FormBuilder) {
  }

  ngOnInit() {

    this.getProduct(this.route.snapshot.params['id']);

    this.categoriesService.getCategories().subscribe(res => {
      console.log(res);
      this.categories = res;
    }, err => {
      console.log(err);
    });

    this.productForm = this.formBuilder.group({
      'name': [null, Validators.required],
      'description': [null, Validators.required],
      'price': [null, Validators.required],
      'category_id': [null, Validators.required]
    });
  }

  getProduct(id) {
    this.productsService.getProduct(id).subscribe(data => {
      this.id = data.id;
      this.productForm.setValue({
        name: data.name,
        description: data.description,
        // price: data.price,
        price: 0,
        category_id: data.category_id
      });
    });
  }

  onFormSubmit(form: NgForm) {
    this.productsService.updateProduct(form, this.id).subscribe(res => {
      console.log(res);
      this.router.navigate(['products/', this.id]);
    }, (err) => {
      console.log(err);
    });
  }

  viewProduct() {
    this.router.navigate(['products/', this.id]);
  }

  back() {
    this.location.back();
  }

}
