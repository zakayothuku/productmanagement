export interface Product {
  id: Number;
  name: String;
  description: String;
  price: Number;
  category_id: Number;
}
