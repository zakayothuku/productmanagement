import {throwError} from 'rxjs/index';
import {HttpErrorResponse} from '@angular/common/http';

export const baseUrl = 'https://staging.quantumfig.com';

export class Handler {

  public static handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred.
      console.error('An error occurred:', error.error.message);
    } else {
      // Server returned an unsuccessful response code.
      console.error(`Error code ${error.status}, ` + `Error Message: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened; please try again later.');
  }

  public static getResponseData(response: Response) {
    return response || {};
  }

}
