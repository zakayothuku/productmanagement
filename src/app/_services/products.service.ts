import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {baseUrl, Handler} from './handler';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = `${baseUrl}/products`;

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  constructor(private http: HttpClient) {
  }

  getProducts(): Observable<any> {
    const url = `${apiUrl}.json`;
    return this.http.get(url, httpOptions).pipe(map(Handler.getResponseData), catchError(Handler.handleError));
  }

  getProduct(id: number): Observable<any> {
    const url = `${apiUrl}/${id}.json`;
    return this.http.get(url, httpOptions).pipe(map(Handler.getResponseData), catchError(Handler.handleError));
  }

  addProduct(data): Observable<any> {
    const url = `${apiUrl}.json`;
    return this.http.post(url, data, httpOptions).pipe(catchError(Handler.handleError));
  }

  updateProduct(data, id: number): Observable<any> {
    const url = `${apiUrl}/${id}.json`;
    return this.http.put(url, data, httpOptions).pipe(catchError(Handler.handleError));
  }

  deleteProduct(id: number): Observable<{}> {
    const url = `${apiUrl}/${id}.json`;
    return this.http.delete(url, httpOptions).pipe(catchError(Handler.handleError));
  }

}
