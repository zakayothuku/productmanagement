import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {baseUrl, Handler} from './handler';
import {Observable} from 'rxjs/index';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

const apiUrl = `${baseUrl}/categories`;

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  constructor(private http: HttpClient) {
  }

  getCategories(): Observable<any> {
    const url = `${apiUrl}.json`;
    return this.http.get(url, httpOptions).pipe(map(Handler.getResponseData), catchError(Handler.handleError));
  }

  getCategory(id: number): Observable<any> {
    const url = `${apiUrl}/${id}.json`;
    return this.http.get(url, httpOptions).pipe(map(Handler.getResponseData), catchError(Handler.handleError));
  }
}
